package com.epam;

import java.util.Arrays;

public class SortingApp {
    public static void main(String[] args) {
        if (args.length == 0 || args.length > 10) throw new IllegalArgumentException();

        int[] array = new int[args.length];

        for (int i = 0; i < args.length; i++) {
            int a = Integer.parseInt(args[i]);
            array[i] = a;
        }

        System.out.println(Arrays.toString(sortArray(array)));
    }

    private static int[] sortArray(int[] array){
        int temp;
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] > array[j]) {
                    temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
        return array;
    }
}