package com.epam;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.runners.Parameterized.*;

@RunWith(Parameterized.class)
public class ParametrizedTest {
    private String[] args;
    private String expected;

    public ParametrizedTest(String[] args, String expected) {
        this.args = args;
        this.expected = expected;
    }

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Parameters
    public static Collection testParams() {
        return Arrays.asList(new Object[][] {
                {new String[]{"7", "3", "6", "3"}, "[3, 3, 6, 7]"},
                {new String[]{"23", "13", "54", "6", "9"}, "[6, 9, 13, 23, 54]"},
                {new String[]{"10", "42", "123", "24"}, "[10, 24, 42, 123]"},
                {new String[]{"14", "8", "25", "33"}, "[8, 14, 25, 33]"}
        });
    }

    @Test
    public void testRegularCase() {
        SortingApp.main(args);
        assertEquals(expected, systemOutRule.getLog().trim());
    }
}
