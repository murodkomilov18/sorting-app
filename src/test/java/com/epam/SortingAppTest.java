package com.epam;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

import static org.junit.Assert.*;

public class SortingAppTest {
    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test(expected = IllegalArgumentException.class)
    public void testZeroArgumentCase(){
        SortingApp.main(new String[0]);
    }

    @Test
    public void testSingleElementArrayCase() {
        String[] args = new String[]{"1"};
        SortingApp.main(args);
        assertEquals("[1]", systemOutRule.getLog().trim());
    }

    @Test
    public void testSortedArraysCase() {
        String[] args = new String[]{"5", "2", "23", "11", "45", "32", "14", "15", "7", "10"};
        SortingApp.main(args);
        assertEquals("[2, 5, 7, 10, 11, 14, 15, 23, 32, 45]", systemOutRule.getLog().trim());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMoreThanTenArgumentCases() {
        String[] args = new String[]{"5 2 23 11 45 32 14 15 7 10 17"};
        SortingApp.main(args);
    }
}